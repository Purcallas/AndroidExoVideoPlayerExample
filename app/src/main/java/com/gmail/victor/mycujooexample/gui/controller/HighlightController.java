package com.gmail.victor.mycujooexample.gui.controller;

import com.gmail.victor.mycujooexample.model.Highlight;

import java.util.ArrayList;

/**
 * FAKE DATA EVENT CONTROLLER
 * Created by victor on 2/10/17.
 */


public class HighlightController {
    ArrayList<Highlight> videoHightlighs;

    public HighlightController() {
        //Test data
        //Usually build from a external url json
        this.videoHightlighs = buildVideoHighlights();
    }

    private ArrayList<Highlight> buildVideoHighlights() {

        ArrayList<Highlight> highlights = new ArrayList<>();
        Highlight highlight1 = new Highlight(1000,"Broadcast start","Quick highlight description");
        Highlight highlight2 = new Highlight(5000,"Hightlight","Quick highlight description");
        Highlight highlight3 = new Highlight(8000,"Swell","Quick highlight description");
        Highlight highlight4 = new Highlight(12000,"Quick run","Quick highlight description");
        Highlight highlight5 = new Highlight(16000,"Second Try","Quick highlight description");
        Highlight highlight6 = new Highlight(25000,"Hightlight","Quick highlight description");

        highlights.add(highlight1);
        highlights.add(highlight2);
        highlights.add(highlight3);
        highlights.add(highlight4);
        highlights.add(highlight5);
        highlights.add(highlight6);

        return highlights;
    }

    public ArrayList<Highlight> getVideoHightlighs() {
        return videoHightlighs;
    }
}
