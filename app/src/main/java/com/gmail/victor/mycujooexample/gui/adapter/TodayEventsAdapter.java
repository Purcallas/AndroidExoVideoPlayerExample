package com.gmail.victor.mycujooexample.gui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.gmail.victor.mycujooexample.R;
import com.gmail.victor.mycujooexample.gui.activity.VideoPlayerActivity;
import com.gmail.victor.mycujooexample.model.Event;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 *  TodayEventsAdapter
 *
 *  @author victor
 *
 *
 *  Today Events Adapter is a simply Event object adapter to show a list of events
 *  Also includes on click interaction in the event holder
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class TodayEventsAdapter extends RecyclerView.Adapter<TodayEventsAdapter.MyViewHolder> {

    private List<Event> horizontalList;
    private Context context;

    /********************
     *  INTERNAL CLASSES
     ********************/

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView eventTypeTextView;
        TextView eventNameTextView;
        ImageView eventImageView;

        private MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            eventTypeTextView = view.findViewById(R.id.highlight_type);
            eventNameTextView = view.findViewById(R.id.highlight_title);
            eventImageView = view.findViewById(R.id.highlight_image);
        }
    }

    /********************
     *  CONSTRUCTOR
     ********************/


    public TodayEventsAdapter(List<Event> horizontalList) {
        this.horizontalList = horizontalList;
    }

    /********************
     *  COMMON LIFE CYCLE
     ********************/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.highlights_horizontal, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if(holder!=null) {

            Resources resources = context.getResources();
            Event event = horizontalList.get(position);

            //Get the id from image..,
            //This case is only useful to stored in drawable images
            //Usually isn't necessary for a image witch comes from a external url.
            final String imageName = event.getImagePath().split("\\.")[0]; //Don't uses the extension for drawables
            String uri = "@drawable/" +imageName;
            final int imageResource = resources.getIdentifier(uri, null, context.getPackageName());

            //Set data in holder
            holder.eventTypeTextView.setText(event.getEventType());
            holder.eventNameTextView.setText(event.getEventName());

            //Set image using picasso
            //Re dimension the image to prevent OOM according dimensions
            Picasso.with(context).load(imageResource).resize(resources.getDimensionPixelSize(R.dimen.higlight_size_width), resources.getDimensionPixelSize(R.dimen.higlight_size_height)).error(R.mipmap.ic_launcher).into(holder.eventImageView);


            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String transitionName = context.getResources().getString(R.string.bg_video_image_transition);
                    Intent intent = new Intent(context, VideoPlayerActivity.class);
                    intent.putExtra("image_name",imageResource);
                    if (holder.eventImageView != null){
                        ActivityOptionsCompat options = ActivityOptionsCompat.
                                makeSceneTransitionAnimation((Activity) context, holder.eventImageView, transitionName);
                        context.startActivity(intent, options.toBundle());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return horizontalList.size();
    }
}
