package com.gmail.victor.mycujooexample.model;

/**
 * Created by victor on 2/10/17.
 */

public class Highlight {
    private long millis;
    private String title;
    private String subtitle;

    public Highlight(int millis, String title, String subtitle) {
        this.millis = millis;
        this.title = title;
        this.subtitle = subtitle;
    }

    public long getMillis() {
        return millis;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }
}
