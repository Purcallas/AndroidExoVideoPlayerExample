package com.gmail.victor.mycujooexample.gui.controller;

import com.gmail.victor.mycujooexample.model.Event;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * FAKE DATA EVENT CONTROLLER
 * Created by victor on 2/10/17.
 */

public class EventController {



    private ArrayList<Event> todayEvents;
    private ArrayList<Event> upcomingEvents;

    public EventController() {

        //Test data
        //Usually build from a external url json
        this.todayEvents = buildTodayEvents();
        this.upcomingEvents = buildUpcomingEvents();
    }


    public ArrayList<Event> getTodayEvents() {
        return todayEvents;
    }

    public ArrayList<Event> getUpcomingEvents() {
        return upcomingEvents;
    }

    private ArrayList<Event> buildTodayEvents() {
        ArrayList<Event> events = new ArrayList<>();
        Event event1 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime(),"img1.jpg");
        Event event2 = new Event("EVENT TYPE","Duane Sharp",new DateTime(),"img2.jpg");
        Event event3 = new Event("EVENT TYPE","Franciscachester",new DateTime(),"img3.jpg");
        Event event4 = new Event("EVENT TYPE","Svalbard & Jon Mayer Islands",new DateTime(),"img4.jpg");
        Event event5 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime(),"img1.jpg");
        Event event6 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime(),"img2.jpg");
        Event event7 = new Event("EVENT TYPE","Franciscachester",new DateTime(),"img3.jpg");
        Event event8 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime(),"img4.jpg");
        Event event9 = new Event("EVENT TYPE","Franciscachester",new DateTime(),"img1.jpg");
        Event event10 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime(),"img2.jpg");

        events.add(event1);
        events.add(event2);
        events.add(event3);
        events.add(event4);
        events.add(event5);
        events.add(event6);
        events.add(event7);
        events.add(event8);
        events.add(event9);
        events.add(event10);

        return events;
    }

    private ArrayList<Event>  buildUpcomingEvents(){
        ArrayList<Event> events = new ArrayList<>();
        Event event1 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime().plusDays(1),"img1.jpg");
        Event event2 = new Event("EVENT TYPE","Duane Sharp",new DateTime().plusDays(1),"img2.jpg");
        Event event3 = new Event("EVENT TYPE","Franciscachester",new DateTime().plusDays(1),"img3.jpg");
        Event event4 = new Event("EVENT TYPE","Svalbard & Jon Mayer Islands",new DateTime().plusDays(2),"img4.jpg");
        Event event5 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime().plusDays(3),"img1.jpg");
        Event event6 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime().plusDays(4),"img2.jpg");
        Event event7 = new Event("EVENT TYPE","Franciscachester",new DateTime().plusDays(6),"img3.jpg");
        Event event8 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime().plusDays(6),"img4.jpg");
        Event event9 = new Event("EVENT TYPE","Franciscachester",new DateTime().plusDays(6),"img1.jpg");
        Event event10 = new Event("EVENT TYPE","Isabelle Cummings",new DateTime().plusDays(6),"img2.jpg");

        events.add(event1);
        events.add(event2);
        events.add(event3);
        events.add(event4);
        events.add(event5);
        events.add(event6);
        events.add(event7);
        events.add(event8);
        events.add(event9);
        events.add(event10);

        return events;
    }
}
