package com.gmail.victor.mycujooexample.gui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.gmail.victor.mycujooexample.R;
import com.gmail.victor.mycujooexample.gui.adapter.VideoHighLightsAdapter;
import com.gmail.victor.mycujooexample.gui.controller.HighlightController;
import com.gmail.victor.mycujooexample.gui.interfaces.VideoPlayer;
import com.gmail.victor.mycujooexample.model.Highlight;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *  VideoPlayerActivity
 *
 *  @author victor
 *
 *
 *  This activity shows the video player and the list of highlights
 *  Uses a Fragment with Video Player Interface to change the video player if is necessary
 *  The fragment is connected via xml in the layout activity_video_player
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class VideoPlayerActivity extends AppCompatActivity{

    private final String STATE_BG_IMAGE = "bgimage";


    @BindView(R.id.video_highligth_recycler_view)
    RecyclerView hightlighsReciclerView;
    @BindView(R.id.highlight_image)
    ImageView bg_image;
    @BindView(R.id.video_background_play)
    ImageView bg_play;
    @BindView(R.id.bg_color)
    ImageView bg_color;

    private VideoPlayer videoFragment;
    private Boolean bgImageIsShown = true;

    //Listen to the progress bar changes
    private Handler handler;

    //Video url media string
    protected String videoUrlString;

    //Highlights seconds array to set the highlight item selected
    private long[] hightlighsSeconds;
    private int actualItemPosition = 0;
    private VideoHighLightsAdapter videoHighLightsAdapter;

    //FadeOut animation
    Animation fadeOut;

    /********************
     *  COMMON LIFE CYCLE
     ********************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        ButterKnife.bind(this);


        //Get image
        Intent intent = getIntent();
        int imageResource = intent.getIntExtra("image_name",0);

        //Set image using picasso
        //Re dimension the image to prevent OOM according dimensions
        bg_image.setImageDrawable(getResources().getDrawable(imageResource,null));
        //Picasso.with(this).load(imageResource).error(R.mipmap.ic_launcher).into(bg_image);

        //Get saved instance state
        if (savedInstanceState != null) {
            bgImageIsShown = savedInstanceState.getBoolean(STATE_BG_IMAGE);
        }

        videoFragment = (VideoPlayer) getSupportFragmentManager().findFragmentById(R.id.video_fragment);

        //Set the url to Stream
        this.videoUrlString = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8";

        //Usually the highlights are in the hls file as metadata
        //Here we use a mock data
        ArrayList<Highlight> highlights = new HighlightController().getVideoHightlighs();
        fillHighlightSecondsArray(highlights);

        //Highlights list
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(VideoPlayerActivity.this, LinearLayoutManager.VERTICAL, false);
        hightlighsReciclerView.setLayoutManager(verticalLayoutmanager);
        videoHighLightsAdapter =new VideoHighLightsAdapter(this,highlights);
        hightlighsReciclerView.setAdapter(videoHighLightsAdapter);



        //Fade out animation
        fadeOut = new AlphaAnimation(1, 0);  // the 1, 0 here notifies that we want the opacity to go from opaque (1) to transparent (0)
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1000); // Fadeout duration should be 1000 milli seconds
        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                bgImageIsShown = false;
                bg_image.setVisibility(View.GONE);
                bg_play.setVisibility(View.GONE);
                bg_color.setVisibility(View.GONE);

            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        //Background image disappear onClick and init the video
        bg_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bg_image.setAnimation(fadeOut);
                bg_play.setAnimation(fadeOut);
                bg_color.setAnimation(fadeOut);
                updateProgress();
                videoFragment.play();
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(STATE_BG_IMAGE, bgImageIsShown);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!bgImageIsShown){
            bg_image.setVisibility(View.GONE);
            bg_play.setVisibility(View.GONE);
            bg_color.setVisibility(View.GONE);
            updateProgress();
            videoFragment.play();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (handler != null) handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    /********************
     *  PUBLIC  METHODS
     ********************/

    public void setPlayerMillis(long millis){

        //Start the video if the background image is already displayed
        if (bg_image.getVisibility() != View.GONE){
            bg_image.setAnimation(fadeOut);
            videoFragment.play();
        }
        //Set the millis
        videoFragment.setVideoMillis(millis);
        getActualHighlightPosition(millis);
    }

    /********************
     *  PRIVATE  METHODS
     ********************/

    private void fillHighlightSecondsArray(ArrayList<Highlight> highlights) {
        hightlighsSeconds = new long[highlights.size()];
        for (int i = 0; i < hightlighsSeconds.length ; i++){
            hightlighsSeconds[i] = highlights.get(i).getMillis();
        }
    }

    // METHODS TO VIDEO & RECYCLER VIEW ITEMS COMMUNICATION

    private int getActualHighlightPosition(long millis){
        int actualPosition = 0;
        for (int i = 0 ; i < hightlighsSeconds.length ; i++){
            if (hightlighsSeconds[i]>millis)break;
            else{
                actualPosition = i;
            }
        }
        if (actualPosition != actualItemPosition){
            actualItemPosition = actualPosition;
            videoHighLightsAdapter.setItemSelected(actualItemPosition);
        }
        return actualPosition;
    }

    // Listen to progress bar
    private void updateProgress(){
        if (handler == null){
            handler = new Handler();
        }else{
            handler.removeCallbacksAndMessages(null);
        }
        //get current progress
        long position = videoFragment.getVideoMillis();
        //updateProgress() will be called repeatedly, you can check
        //player state to end it

        getActualHighlightPosition(position);


        Log.v(this.getClass().getName(),""+position);
        handler.postDelayed(updateProgressAction,1000);

    }

    private final Runnable updateProgressAction = new Runnable() {
        @Override
        public void run() {
            updateProgress();
        }
    };


}
