package com.gmail.victor.mycujooexample.gui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gmail.victor.mycujooexample.R;
import com.gmail.victor.mycujooexample.gui.adapter.TodayEventsAdapter;
import com.gmail.victor.mycujooexample.gui.adapter.UpcomingEventsAdapter;
import com.gmail.victor.mycujooexample.gui.controller.EventController;
import com.gmail.victor.mycujooexample.model.Event;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *  MainActivity
 *
 *  @author victor
 *
 *
 *  This activity two recyclerviews with the today events and upcomming events.
 *  Uses simply two adapters with linear layout manager
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class MainActivity extends AppCompatActivity {

    //Horizontal and Vertical List Views
    @BindView(R.id.vertical_recycler_view)
    RecyclerView vertical_recycler_view;
    @BindView(R.id.horizontal_recycler_view)
    RecyclerView horizontal_recycler_view;

    /********************
     *  COMMON LIFE CYCLE
     ********************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Init Butter Knife
        ButterKnife.bind(this);

        //Get the data from the controller
        EventController eventController = new EventController();
        ArrayList<Event> horizontalList = eventController.getTodayEvents();
        ArrayList<Event> verticalList = eventController.getUpcomingEvents();


        TodayEventsAdapter todayEventsAdapter = new TodayEventsAdapter(horizontalList);
        UpcomingEventsAdapter upcomingEventsAdapter = new UpcomingEventsAdapter(verticalList);


        //Highlights list
        LinearLayoutManager verticalLayoutmanager
                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
        vertical_recycler_view.setLayoutManager(verticalLayoutmanager);

        //Events list
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

        vertical_recycler_view.setAdapter(upcomingEventsAdapter);
        horizontal_recycler_view.setAdapter(todayEventsAdapter);
    }

}