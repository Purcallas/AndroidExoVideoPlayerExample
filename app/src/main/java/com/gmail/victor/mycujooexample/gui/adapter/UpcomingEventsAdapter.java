package com.gmail.victor.mycujooexample.gui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gmail.victor.mycujooexample.R;
import com.gmail.victor.mycujooexample.model.Event;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 *  Upcoming Events Adapter
 *
 *  @author victor
 *
 *
 *  Upcoming Events Adapter is a simply Event object adapter to show a list of events
 *  Shows a toast with the Event name on click
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class UpcomingEventsAdapter extends RecyclerView.Adapter<UpcomingEventsAdapter.MyViewHolder> {

    private List<Event> upcomingEvents;
    private Context context;

    /********************
     *  INTERNAL CLASSES
     ********************/

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView eventTypeTextView;
        TextView eventNameTextView;
        TextView eventDateTextView;
        ImageView eventImageView;

        private MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            eventTypeTextView = view.findViewById(R.id.upcomming_type);
            eventNameTextView = view.findViewById(R.id.upcoming_title);
            eventDateTextView = view.findViewById(R.id.upcomming_date);
            eventImageView = view.findViewById(R.id.upcoming_image);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(context, eventNameTextView.getText().toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {
        return upcomingEvents.size();
    }

    /********************
     *  CONSTRUCTOR
     ********************/

    public UpcomingEventsAdapter(List<Event> upcomingEvents) {
        this.upcomingEvents = upcomingEvents;
    }

    /********************
     *  COMMON LIFE CYCLE
     ********************/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_vertical, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UpcomingEventsAdapter.MyViewHolder holder, final int position) {
        if(holder!=null) {

            Resources resources = context.getResources();
            Event event = upcomingEvents.get(position);

            //Get the id from image..,
            //This case is only useful to stored in drawable images
            //Usually isn't necessary for a image witch comes from a external url.
            String imageName = event.getImagePath().split("\\.")[0]; //Don't uses the extension for drawables
            String uri = "@drawable/" +imageName;
            int imageResource = resources.getIdentifier(uri, null, context.getPackageName());

            //Set data in holder
            holder.eventTypeTextView.setText(event.getEventType());
            holder.eventNameTextView.setText(event.getEventName());
            holder.eventDateTextView.setText(event.getDateTime().toString());

            //Set image using picasso
            //Re dimension the image to prevent OOM according dimensions
            Picasso.with(context).load(imageResource).resize(resources.getDimensionPixelSize(R.dimen.higlight_size_width), resources.getDimensionPixelSize(R.dimen.higlight_size_height)).error(R.mipmap.ic_launcher).into(holder.eventImageView);

        }
    }
}