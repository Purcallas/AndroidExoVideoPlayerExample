package com.gmail.victor.mycujooexample.gui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gmail.victor.mycujooexample.R;
import com.gmail.victor.mycujooexample.gui.activity.VideoPlayerActivity;
import com.gmail.victor.mycujooexample.model.Highlight;

import java.util.List;

/**
 *  Video Highlights Adapter
 *
 *  @author victor
 *
 *
 *  Upcoming Events Adapter is a simply Highlight object adapter to show a list of highlights
 *  Changes the state of the holder view to selected (changing his ui) on click.
 *  Also sends to the Activity (Parent) the event seconds on click on it
 *  Also has a public receiver to change the actual selected element on recyclerview list
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class VideoHighLightsAdapter extends RecyclerView.Adapter<VideoHighLightsAdapter.MyViewHolder> {

    private List<Highlight> verticalList;
    // Start with first item selected
    private int focusedItem = 0;
    private VideoPlayerActivity exoVideoPlayerActivity;

    /********************
     *  INTERNAL CLASSES
     ********************/

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView hightlightTitleTextView;
        TextView higthlightSubtitleTextView;
        TextView higthlightSecondsTextView;
        ImageView highlightSelectedSquare;
        RelativeLayout hightlightLayout;

        private MyViewHolder(View view) {
            super(view);
            hightlightTitleTextView = view.findViewById(R.id.video_highlight_title);
            higthlightSubtitleTextView = view.findViewById(R.id.video_highlight_subtitle);
            higthlightSecondsTextView = view.findViewById(R.id.video_highlight_seconds);
            highlightSelectedSquare = view.findViewById(R.id.video_highlight_image);
            hightlightLayout = view.findViewById(R.id.highlight_layout);

        }
    }

    /********************
     *  CONSTRUCTOR
     ********************/

    public VideoHighLightsAdapter(VideoPlayerActivity exoVideoPlayerActivity, List<Highlight> verticalList) {
        this.verticalList = verticalList;
        this.exoVideoPlayerActivity = exoVideoPlayerActivity;
    }

    /********************
     *  COMMON LIFE CYCLE
     ********************/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_highlighs_vertical, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final VideoHighLightsAdapter.MyViewHolder holder, final int position) {
        if(holder!=null) {

            holder.itemView.setSelected(focusedItem == position);

            final Highlight highlight = verticalList.get(position);

            //Set data in holder
            holder.hightlightTitleTextView.setText(highlight.getTitle());
            holder.higthlightSubtitleTextView.setText(highlight.getSubtitle());

            //Seconds text
            String seconds = highlight.getMillis()/1000 + "\'";
            holder.higthlightSecondsTextView.setText(seconds);

            if (holder.itemView.isSelected()){
                holder.hightlightTitleTextView.setSelected(true);
                holder.higthlightSubtitleTextView.setSelected(true);
                holder.hightlightLayout.setSelected(true);
            }else{
                holder.hightlightTitleTextView.setSelected(false);
                holder.higthlightSubtitleTextView.setSelected(false);
                holder.hightlightLayout.setSelected(false);
            }

            // Handle item click and set the selection
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Redraw the old selection and the new
                    setItemSelected(holder.getLayoutPosition());
                    exoVideoPlayerActivity.setPlayerMillis(highlight.getMillis());
                }
            });


        }
    }

    @Override
    public int getItemCount() {
        return verticalList.size();
    }

    @Override
    public void onAttachedToRecyclerView(final RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        // Handle key up and key down and attempt to move selection
        recyclerView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                RecyclerView.LayoutManager lm = recyclerView.getLayoutManager();

                // Return false if scrolled to the bounds and allow focus to move off the list
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                        return tryMoveSelection(lm, 1);
                    } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        return tryMoveSelection(lm, -1);
                    }
                }

                return false;
            }
        });
    }

    /********************
     *  PUBLIC METHODS
     ********************/

    // Redraw the old selection and the new
    public void setItemSelected(int position){
        notifyItemChanged(focusedItem);
        focusedItem = position;
        notifyItemChanged(focusedItem);
    }

    /********************
     *  PRIVATE METHODS
     ********************/

    private boolean tryMoveSelection(RecyclerView.LayoutManager lm, int direction) {
        int tryFocusItem = focusedItem + direction;

        // If still within valid bounds, move the selection, notify to redraw, and scroll
        if (tryFocusItem >= 0 && tryFocusItem < getItemCount()) {
            notifyItemChanged(focusedItem);
            focusedItem = tryFocusItem;
            notifyItemChanged(focusedItem);
            lm.scrollToPosition(focusedItem);
            return true;
        }

        return false;
    }

}