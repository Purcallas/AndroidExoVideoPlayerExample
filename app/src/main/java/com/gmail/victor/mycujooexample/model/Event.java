package com.gmail.victor.mycujooexample.model;

import org.joda.time.DateTime;

/**
 * Created by victor on 2/10/17.
 */

public class Event {
    private String eventType;
    private String eventName;
    private DateTime dateTime;
    private String imagePath;

    public Event(String eventType, String eventName, DateTime dateTime, String imagePath) {
        this.eventType = eventType;
        this.eventName = eventName;
        this.dateTime = dateTime;
        this.imagePath = imagePath;
    }

    public String getEventType() {
        return eventType;
    }

    public String getEventName() {
        return eventName;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public String getImagePath() {
        return imagePath;
    }
}
