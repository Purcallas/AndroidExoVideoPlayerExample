package com.gmail.victor.mycujooexample.gui.fragment;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.gmail.victor.mycujooexample.R;
import com.gmail.victor.mycujooexample.gui.interfaces.VideoPlayer;
import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *  Exo Video Player Fragment
 *
 *  @author victor
 *
 *
 *  This fragment uses ExoPlayer Google Library to display and interact with the hls
 *  streaming videos
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public class ExoVideoPlayerFragment extends Fragment implements VideoRendererEventListener,VideoPlayer {

    private final String STATE_RESUME_WINDOW = "resumeWindow";
    private final String STATE_RESUME_POSITION = "resumePosition";
    private final String STATE_PLAYER_FULLSCREEN = "playerFullscreen";

    @BindView(R.id.player_view)
    SimpleExoPlayerView simpleExoPlayerView;
    @BindView(R.id.main_media_frame)
    FrameLayout playerFrame;
    @BindView(R.id.exo_fullscreen_button)
    FrameLayout fullScreenButton;
    @BindView(R.id.exo_fullscreen_icon)
    ImageView fullScreenIcon;

    //Video player variables
    private SimpleExoPlayer player;
    private MediaSource videoSource;
    private Dialog fullScreenDialog;
    private boolean isFullscreen = false;
    private int resumeWindow;
    private long resumePosition;
    private boolean isInitial = true;

    //String with url
    protected String videoUrlString;

    /********************
     *  COMMON LIFE CYCLE
     ********************/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get saved instance state
        if (savedInstanceState != null) {
            resumeWindow = savedInstanceState.getInt(STATE_RESUME_WINDOW);
            resumePosition = savedInstanceState.getLong(STATE_RESUME_POSITION);
            isFullscreen = savedInstanceState.getBoolean(STATE_PLAYER_FULLSCREEN);
        }

        this.videoUrlString = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8";

    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.exo_player_video_layout, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onPause() {

        super.onPause();
        resumeWindow = player.getCurrentWindowIndex();
        resumePosition = Math.max(0, player.getContentPosition());

        if (simpleExoPlayerView != null && simpleExoPlayerView.getPlayer() != null) {
            player.release();
        }

        if (fullScreenDialog != null)
            fullScreenDialog.dismiss();
    }

    @Override
    public void onResume() {

        super.onResume();

        if (videoSource == null) {

            initFullscreenDialog();
            initFullscreenButton();

            //Static video. Usually the video stream must be contained in an object
            String userAgent = Util.getUserAgent(this.getContext(), getContext().getApplicationInfo().packageName);
            DefaultHttpDataSourceFactory httpDataSourceFactory = new DefaultHttpDataSourceFactory(userAgent, null, DefaultHttpDataSource.DEFAULT_CONNECT_TIMEOUT_MILLIS, DefaultHttpDataSource.DEFAULT_READ_TIMEOUT_MILLIS, true);
            DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this.getContext(), null, httpDataSourceFactory);
            Uri daUri = Uri.parse(videoUrlString);

            videoSource = new HlsMediaSource(daUri, dataSourceFactory, 1, null, null);
        }

        //Init Exo Player
        initExoPlayer();

        //Adapt to full screen if necessary
        if (isFullscreen) {
            ((ViewGroup) simpleExoPlayerView.getParent()).removeView(simpleExoPlayerView);
            fullScreenDialog.addContentView(simpleExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            fullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.ic_fullscreen_skrink));
            fullScreenDialog.show();
        }
    }

    /********************
     *  PRIVATE METHODS
     ********************/

    private void initFullscreenDialog() {

        fullScreenDialog = new Dialog(this.getContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen) {
            public void onBackPressed() {
                if (isFullscreen)
                    closeFullscreenDialog();
                super.onBackPressed();
            }
        };
    }


    private void openFullscreenDialog() {

        ((ViewGroup) simpleExoPlayerView.getParent()).removeView(simpleExoPlayerView);
        fullScreenDialog.addContentView(simpleExoPlayerView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        fullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.ic_fullscreen_skrink));
        isFullscreen = true;
        fullScreenDialog.show();
    }


    private void closeFullscreenDialog() {

        ((ViewGroup) simpleExoPlayerView.getParent()).removeView(simpleExoPlayerView);
        playerFrame.addView(simpleExoPlayerView);
        isFullscreen = false;
        fullScreenDialog.dismiss();
        fullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this.getContext(), R.drawable.ic_fullscreen_expand));
    }


    private void initFullscreenButton() {
        fullScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isFullscreen)
                    openFullscreenDialog();
                else
                    closeFullscreenDialog();
            }
        });
    }


    private void initExoPlayer() {
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        LoadControl loadControl = new DefaultLoadControl();
        player = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this.getContext()), trackSelector, loadControl);

        simpleExoPlayerView.setPlayer(player);

        boolean haveResumePosition = resumeWindow != C.INDEX_UNSET;

        if (haveResumePosition) {
            player.seekTo(resumeWindow,  resumePosition);
        }


        final LoopingMediaSource loopingSource = new LoopingMediaSource(videoSource);
        player.prepare(loopingSource);
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {}

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {}

            @Override
            public void onLoadingChanged(boolean isLoading) {}

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {}

            @Override
            public void onRepeatModeChanged(int repeatMode) {}

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                player.stop();
                player.prepare(loopingSource);
                player.setPlayWhenReady(true);
            }

            @Override
            public void onPositionDiscontinuity() {}

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {}
        });


        player.setVideoDebugListener(this); //fit the resolution
        player.setPlayWhenReady(!isInitial);      //Don't start the video
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_RESUME_WINDOW, resumeWindow);
        outState.putLong(STATE_RESUME_POSITION,  resumePosition);
        outState.putBoolean(STATE_PLAYER_FULLSCREEN, isFullscreen);

        super.onSaveInstanceState(outState);
    }

    /*********************************
     *  VIDEO EVENT LISTENER INTERFACE
     **********************************/


    @Override
    public void onVideoEnabled(DecoderCounters counters) {

    }

    @Override
    public void onVideoDecoderInitialized(String decoderName, long initializedTimestampMs, long initializationDurationMs) {

    }

    @Override
    public void onVideoInputFormatChanged(Format format) {

    }

    @Override
    public void onDroppedFrames(int count, long elapsedMs) {

    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees, float pixelWidthHeightRatio) {

    }

    @Override
    public void onRenderedFirstFrame(Surface surface) {

    }

    @Override
    public void onVideoDisabled(DecoderCounters counters) {

    }

    /*********************************
     *  VIDEO PLAYER INTERFACE
     **********************************/
    @Override
    public long getVideoMillis() {
        return player == null?0:player.getCurrentPosition();
    }

    @Override
    public void setVideoMillis(long videoMillis) {
        if(player!=null){
            player.seekTo(videoMillis);
            simpleExoPlayerView.showController();
        }
    }

    @Override
    public void play() {
        isInitial = false;
        onResume();
    }
}
