package com.gmail.victor.mycujooexample.gui.interfaces;

/**
 *  Video Player Interface
 *
 *  @author victor
 *
 *
 *  Provides the neccessary methods to Video Player Activity to interact with
 *  any Video Player Fragment (Exo Video Player Fragment in that case)
 *
 *
 *  LICENSE
 *
 *  This file is part of MycujooExample.
 *
 *  MycujooExample is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MycujooExample is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MycujooExample.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

public interface VideoPlayer {

    long getVideoMillis();
    void setVideoMillis(long videoMillis);
    void play();
}
